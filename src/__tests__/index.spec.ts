import { sum } from '../index';

test('Sum 5 + 6 = 11', () => {
  expect(sum(4, 5)).toBe(9);
});
